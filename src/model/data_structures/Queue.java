package model.data_structures;

import java.util.Iterator;

public class Queue<T> implements IQueue<T> 
{
	
	private Node<T> firstNode, lastNode;

	private int listSize;

	
	public Queue() {
		firstNode = null;
		lastNode = null;
	}

	

	public Iterator<T> iterator() {

		return new Iterator<T>() {
			Node<T> act = null;

			
			public boolean hasNext() {
				if (listSize == 0) {
					return false;
				}

				if (act == null) {
					return true;
				}

				return act.getNext() != null;
			}

			
			public T next() {
				if (act == null) {
					act = firstNode;
				} else {
					act = act.getNext();
				}

				return act.getElement();
			}

		};
	}

	
	public boolean isEmpty() {
		return firstNode == null;
	}


	public int size() {
		return listSize;
	}

	
	public void enqueue(T t) {
		Node<T> oldLast = lastNode;
		lastNode = new Node<>(t);

		if(isEmpty()) {
			firstNode = lastNode;
		} else {
			oldLast.setNext(lastNode);
		}

		listSize ++;
	}

	
	public T dequeue() {
		if (!isEmpty()) {
			T element = firstNode.getElement();
			firstNode = firstNode.getNext();

			listSize --;

			return element;
		}

		return null;
	}
}