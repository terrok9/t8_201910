package model.data_structures;

public class Node<T> {
	
	public Node(T element) {
		
	    element = (T) this;
	}
	
	public T getElement() {
		return (T) this;
	}

	
	public Node<T> getNext() {
		return this.getNext();
	}

	
	public void setElement(T element) {
		element = (T) this;
	}

	public void setNext(Node<T> next) {
		next = this;
	}
}