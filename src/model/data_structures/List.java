package model.data_structures;

import java.util.Iterator;

public class List<T extends Comparable<T>> implements IList<T> {

	private Object[] elements;

	private int listSize;

	public List() {
		elements = new Object[100];
		listSize = 0;
	}

	
	public void add(T elem) {
		if (listSize == elements.length) resize(2 * elements.length);
		elements[listSize++] = elem;

	}

	
	public T remove(T elem) {

		if (listSize == 0) return null;

		Iterator<T> iter = iterator();
		int counter = 0;
		T removedElement = null;

		while(iter.hasNext()) {
			T e = iter.next();

			if (e == null) {
				return null;
			}

			if(e.compareTo(elem) == 0) {
				removedElement = (T) elements[counter];
				elements[counter] = null;

				for(int i = counter; i < listSize; ) {
					elements[i] = elements[++i];

					if(i + 1 == listSize) {
						elements[i++] = null;
					}
				}

				listSize --;

				if (listSize > 0 && listSize == elements.length/4) resize(elements.length/2);

				break;
			}

			counter++;
		}

		return removedElement;
	}

	
	public int size() {
		return listSize;
	}

	
	public T get(T elem) {
		Iterator<T> iter = iterator();
		T element = null;

		while(iter.hasNext()) {
			T e = iter.next();

			if (e == null) {
				return null;
			}

			if(e.compareTo(elem) == 0) {
				element = e;
				break;
			}
		}

		return element;
	}

	
	public T get(int pos) {
		return (T) elements[pos];
	}

	
	public boolean isEmpty() {
		return listSize == 0;
	}

	
	public Iterator<T> iterator() {
		return new Iterator<T>() {
			T act = null;
			int pos = 0;

			
			public boolean hasNext() {
				if (isEmpty()) {
					return false;
				}

				if (act == null) {
					return true;
				}
				
				return ++pos < listSize;
			}

			
			public T next() {
				act = (T) elements[pos];

				return act;
			}
		};
	}

	
	private void resize(int capacity) {
		Object[] copy = new Object[capacity];

		for (int i = 0; i < listSize; i++) {
			copy[i] = (T) elements[i];
		}

		elements = copy;
	}

	
	public void set(int pos, T elem) {
		elements[pos] = elem;
	}

}