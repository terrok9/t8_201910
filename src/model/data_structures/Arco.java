package model.data_structures;

public class Arco<K extends Comparable<K>, A> implements Comparable<Arco<K,A>> {
	
	private K vertice1;
	private K vertice2;
	private A info;
	
	public Arco(K verticeOrigen, K verticeDestino, A info) {
		this.vertice1 = verticeOrigen;
		this.vertice2 = verticeDestino;
		this.info = info;
	}
	
	public K darVertice1() {
		return vertice1;
	}
	
	public K darVertice2() {
		return vertice2;
	}
	
	public A darInfo() {
		return info;
	}
	
	public void setInfo(A info) {
		this.info = info;
	}

	@Override
	public int compareTo(Arco<K, A> o) {
		String key1 = vertice1.toString() + "-" + vertice2.toString();
		String key2 = o.darVertice1().toString() + "-" + o.darVertice2().toString();
		
		if(key1.compareTo(key2) == 0) {
			return 0;
		} else if(key1.compareTo(key2) < 0) {
			return -1;
		} else {
			return 1;
		}
	}
}