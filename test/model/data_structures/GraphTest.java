package model.data_structures;


import org.junit.*;

import model.data_structures.Graph;

class GraphTest {

	
	class VerticePrueba {
		int id;
		String cadena;

		public VerticePrueba(int id, String cadena) {
			this.id = id;
			this.cadena = cadena;
		}
	}

	class ArcoPrueba {
		String arco;

		public ArcoPrueba(String arco) {
			this.arco = arco;
		}
	}

	
	private Graph<Integer, VerticePrueba, ArcoPrueba> grafo;

	public void setupEscenario1() {
		grafo = new Graph<>();
	}

	public void setupEscenario2() {
		grafo = new Graph<>();

		VerticePrueba vertice1 = new VerticePrueba(1, "Vertice1");
		VerticePrueba vertice2 = new VerticePrueba(2, "Vertice2");
		VerticePrueba vertice3 = new VerticePrueba(3, "Vertice3");
		VerticePrueba vertice4 = new VerticePrueba(4, "Vertice4");
		VerticePrueba vertice5 = new VerticePrueba(5, "Vertice5");

		grafo.addVertex(vertice1.id, vertice1);
		grafo.addVertex(vertice2.id, vertice2);
		grafo.addVertex(vertice3.id, vertice3);
		grafo.addVertex(vertice4.id, vertice4);
		grafo.addVertex(vertice5.id, vertice5);

		ArcoPrueba arco1 = new ArcoPrueba(vertice1.cadena + "-" + vertice2.cadena);
		ArcoPrueba arco2 = new ArcoPrueba(vertice2.cadena + "-" + vertice3.cadena);
		ArcoPrueba arco3 = new ArcoPrueba(vertice3.cadena + "-" + vertice4.cadena);
		ArcoPrueba arco4 = new ArcoPrueba(vertice4.cadena + "-" + vertice5.cadena);

		grafo.addEdge(1, 2, arco1);
		grafo.addEdge(2, 3, arco2);
		grafo.addEdge(3, 4, arco3);
		grafo.addEdge(4, 5, arco4);
	}

	void testAddVertex() {
		setupEscenario1();
		grafo.addVertex(1, new VerticePrueba(1, "Vertice1"));
		assertEquals(1, grafo.V(), "No se agreg� al grafo");
		assertEquals("Vertice1", grafo.getInfoVertex(1).cadena, "El vertice no existe");
	}

	
	void testAddEdge() {
		setupEscenario2();

		ArcoPrueba arco = new ArcoPrueba("Vertice1-Vertice3");
		grafo.addEdge(1, 3, arco);
		assertEquals(5, grafo.E(), "No se agreg� al grafo");
		assertEquals("Vertice1-Vertice3", grafo.getInfoArc(1, 3).arco);
	}

	
	void testGetInfoVertex() {
		//Caso 1
		setupEscenario1();
		assertNull(grafo.getInfoVertex(1), "El vertice no debe existir");

		//Caso 2
		setupEscenario2();
		assertNotNull(grafo.getInfoVertex(1), "El vertice deber�a existir");
		assertEquals("Vertice1", grafo.getInfoVertex(1).cadena, "El vertice no es correcto");

		//Caso 3
		assertNull(grafo.getInfoVertex(6), "El vertice no deber�a existir");
	}

	
	void testSetInfoVertex() {
		setupEscenario2();
		
		VerticePrueba vertice = grafo.getInfoVertex(1);
		vertice.cadena = "Vertice1 Modificado";
		grafo.setInfoVertex(vertice.id, vertice);
		
		assertEquals("Vertice1 Modificado", grafo.getInfoVertex(1).cadena, "El vertice no fue modificado");
	}

	
	
	void testGetInfoArc() {
		//Caso 1
		setupEscenario1();
		assertNull(grafo.getInfoArc(1, 2), "El arco no debe existir");

		//Caso 2
		setupEscenario2();
		assertNotNull(grafo.getInfoArc(1, 2), "El arco deber�a existir");
		assertEquals("Vertice1-Vertice2", grafo.getInfoArc(1, 2).arco, "El arco no es correcto");

		//Caso 3
		assertNull(grafo.getInfoArc(1, 5), "El arco no deber�a existir");
	}

	
	void testSetInfoArc() {
		setupEscenario2();
		
		ArcoPrueba arco = grafo.getInfoArc(1, 2);
		arco.arco = "Vertice1-Vertice2 Modificado";
		grafo.setInfoArc(1, 2, arco);
		
		assertEquals("Vertice1-Vertice2 Modificado", grafo.getInfoArc(1, 2).arco, "No se modific� el arco");
	}

}
